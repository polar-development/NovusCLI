<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://i.imgur.com/hKwFkEZ.png" alt="Project logo"></a>
</p>

<h3 align="center">Novus Command Line Edition.</h3>

<div align="center">

  ![Languages](https://img.shields.io/github/languages/count/Official-polar-team/NovusCLI.svg)
  ![Repository Size](https://img.shields.io/github/repo-size/Official-polar-team/NovusCLI.svg)
  ![Code Size](https://img.shields.io/github/languages/code-size/Official-polar-team/NovusCLI.svg)
  ![Forks](https://img.shields.io/github/forks/Official-polar-team/NovusCLI.svg) 
  ![GitHub Issues](https://img.shields.io/github/issues/Official-polar-team/NovusCLI.svg)
  ![GitHub Stars](https://img.shields.io/github/stars/Official-polar-team/NovusCLI.svg)
  ![License](https://img.shields.io/github/license/Official-polar-team/NovusCLI.svg)

</div>

---

<p align="center"> The best APT commandline enhancement!
    <br> 
</p>

## 📝 Table of Contents
- [About](#about)
- [Getting Started](#getting_started)
- [How to Build](#building)
- [How to Install](#installing)
- [Usage](#usage)
- [Built Using](#built_using)
- [Contributing](../CONTRIBUTING.md)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)

## 🧐 About <a name = "about"></a>
NovusCLI is made to make using APT less annoying by providing an easy to use set of commands with syntax highlighting that never require you to ever have to type sudo or worry about case sensitivity!

## 🏁 Getting Started <a name = "getting_started"></a>
Getting started for contributing to NovusCLI is very easy, you just need to follow the instructions below if you wish to build from source.

### Prerequisites
In order to start building NovusCLI, you are going to need to have rustup and cargo installed (Installing rustup automatically installs cargo so just worry about rustup). You can get rustup [here](https://www.rust-lang.org/tools/install/).

Without [APT](https://launchpad.net/ubuntu/+source/apt/) (Linux) or [MacPT](https://github.com/Official-polar-team/MacPT/) (macOS) it's useless though so be sure to install what fits for your system!

## Building

* cd into the nvs directory (You can easily do so by opening the terminal typing ```cd``` and dragging and dropping the nvs folder into the terminal window and clicking the enter key).
* Run ```cargo build --release``` in the terminal.
* Make sure your in the nvs directory and from there go into the target directory and then go into the release directory (nvs --> target --> release).
* You should see a executable file named "nvs" without the "".
* Congratulations! You have successfully built NovusCLI (nvs)!

## Installing
### Using MacPT

* If you have MacPT installed on your machine you just have to make sure you have the Serna repository on your machine. To check run ```sudo apt edit-sources``` and press enter, (If you have a password set on your Mac it will ask you to input your password, when you type it won't show you're typing but you are typing so just type your password and click the enter key) if you get an error it's most likely because you don't have a nano installed, [click here](#building-and-installing-nano) and follow the tutorial to install nano.
* After entering ```sudo apt edit-sources``` you should see ```deb https://sernarepo.com/macos georgia main``` on it's own line in there. If you don't see it there then add it! To save your changes press the Control + O without the + and then click enter and to exit press Control + X without the +.
* If you didn't see the repository there and you had to manually add it then type ```sudo apt update``` in your terminal and click enter (If you have a password set on your mac it will ask you to input your password, when you type it won't show you're typing but you are typing so just type your password and click the enter key).
* Now type ```sudo apt install novuscli```.
* Congratulations! You have successfully installed NovusCLI (nvs)!

#### Building and Installing Nano

* [Click here](https://nano-editor.org/download.php/) to go download the nano source (We recommend the tar.xz one)
* Go to the folder it was downloaded to (By default it downloads to ~/Downloads but if you changed the directory where files * download then just go there).
* Extract the nano source you downloaded.
* Open your terminal
* Type ```cd``` then drag and drop the folder you extracted and click enter.
* run ```./configure```.
* run ```make```.
* run ```sudo make install```.

### Manually
#### Linux and macOS
* If you haven't already go build NovusCLI, for instructions on that [clicking here](#Building).
* Make sure where the application name is displayed on the mac's top bar that is says ```Finder```, if it doesn't click on your desktop background or the finder icon on your dock. After doing so hold the keyboard shortcut Command + Shift + G (Or just navigate manually) a text field asking for a file path should appear (If anything is in the field clear it) in that text field type ```/usr/local/bin/``` and click enter.
* Drag the nvs binary you got from compiling novus in the window that just popped up.
* Congratulations! You have successfully installed NovusCLI (nvs)! Without [APT](https://launchpad.net/ubuntu/+source/apt/) (Linux) or [MacPT](https://github.com/Official-polar-team/MacPT/) (macOS) it's useless though so be sure to install what fits for your system!
* Whenever a new NovusCLI update is out just [rebuild](#Building), navigate to ```/usr/local/bin/``` and replace the old ```nvs``` binary with the new ```nvs``` binary you just rebuilt.

#### Windows 10
* If you haven't already go build NovusCLI, for instructions on that [clicking here](#Building).
* Navigate to your ```C:``` directory (This should be your very last directory shown in the file explorer besides ```This PC```) and create a new folder named ```bin```.
* Put the ```nvs``` binary in the ```bin``` folder.
* In the ```Type here to search``` box search ```Edit the system environment variables``` and open the control center program.
* Click ```Envirnment Variables..```.
* Under ```System variables``` (The bottom box with the list of variables) look for the variable named ```path``` and click on it then click ```Edit...``` (A window named ```Edit envirnment variable``` should pop up).
* Click ```New``` and input ```C:\bin``` for the new entry. After you do that click ```OK``` for all the windows related to ```Edit envirnment variable```.
* In the ```Type here to search``` box search ```Registry Editor``` and open the Registry Editor app (If it asks if you want to allow the app to make changes to your device choose ```Yes```).
* Click the drop down arrow for ```HKEY_CURRENT_USER``` and select the folder named ```Console```.
* In the box where it says ```Name```, ```Type```, and ```Data``` look for the name ```VirtualTerminalLevel``` (Under ```Name``` of course). If you don't see ```VirtualTerminalLevel``` right click in the box, hover over ```New``` and click ```DWORD (32-bit) Value``` and give it the name ```VirtualTerminalLevel```.
* Right click on ```VirtualTerminalLevel``` and click ```Modify...``` (A window named ```Edit DWORD (32-bit) Value``` should pop up).
* Make sure the ```Value name:``` is ```VirtualTerminalLevel```, make sure the ```Value data:``` is ```1```, and make sure the ```Base``` has ```Hexadecimal``` selected. After all that information has been properly set click ```OK``` and exit the Registry Editor app (This registry option has to be set to ```1``` so ANSI mode is enabled by default so text coloring properly works with NovusCLI, if you want to revert this back just set the ```Value data:``` to ```0``` but if you do choose to revert back coloring in NovusCLI will break and the text will be hard to read because the escape sequences will be shown).
* Congratulations! You have successfully installed NovusCLI (nvs)! Without [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10) (The Linux subsystem for Windows) NovusCLI will be useless so make sure you install WSL and make sure the Linux distribution you use with WSL has [APT](https://launchpad.net/ubuntu/+source/apt/) installed.
* Whenever a new NovusCLI update is out just [rebuild](#Building), navigate to ```C:\bin``` and replace the old ```nvs``` binary with the new ```nvs``` binary you just rebuilt.

## 🎈 Usage <a name="usage"></a>
NovusCLI commands:

* ```nvs search <query>``` - Searches for specified query
* ```nvs list [--flag(s)] <package(s)>```  - Lists specified packages
* ```nvs info [package(s)]``` - Display info on specified package(s)
* ```nvs install [package(s)]``` - Installs specified package(s)
* ```nvs reinstall [package(s)]``` - Reinstalls specified package(s)
* ```nvs remove [package(s)]``` - Removes specified package(s)
* ```nvs add-key <filepath>``` - Adds a key to the list of trusted keys
* ```nvs edit-sources``` - Opens the APT repo editor
* ```nvs add-repositories <repo(s)>``` - Adds new repo(s)
* ```nvs add-source``` - Adds a new repository
* ```nvs autoremove``` - Removes unneeded packages (orphans)
* ```nvs update``` - Updates the repository lists
* ```nvs upgrade [package(s)]``` - Upgrades specified packages
* ```nvs full-upgrade <package(s)>``` - Upgrades the system (or package(s)) (Linux Only)
* ```nvs version``` - Display APT, DPKG, and NovusCLI versions
* ```nvs clean``` - Clear the download cache
* ```nvs help <command>``` - Opens help menu for specified commands
* ```nvs about``` - View legal information and credits

## ⛏️ Built Using <a name = "built_using"></a>
- [RustLang](https://www.rust-lang.org/) - The only used programming language.
- [MacPT](https://github.com/DiegoMagdaIeno/MacPT) - Backend/Package manager.
- [Project Serna](https://sernarepo.com/) - Main repository.

## ✍️ Authors <a name = "authors"></a>
- [@DiegoMagdaleno](https://github.com/DiegoMagdaIeno) - Initial C++ build.
- [@Diatrus](https://github.com/Diatrus) - Fixing grammar for initial C++ build and fixed a text inconsistency for the rust rewrite (Novus CLI to NovusCLI for the top of the help menu).
- [@SmushyTaco](https://github.com/realSmushyTaco) - Rust rewrite.

See also the list of [contributors](https://github.com/Official-polar-team/NovusCLI/graphs/contributors) who participated in this project.

## 🎉 Acknowledgements <a name = "acknowledgement"></a>
- [PacAPT by ICY](https://github.com/icy/pacapt)
