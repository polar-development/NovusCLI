#!/bin/bash
script_full_path=$(dirname "$0")
cd $script_full_path/nvs
cargo build --release 2> /dev/null
VERSION=$(./target/release/nvs version | awk -F' ' 'NR == 1 {print $2}')
echo "Packaging NovusCLI $VERSION..."
cd .. && rm -rf novuscli_*
mkdir "novuscli_"$VERSION"_darwin-amd64" && cd "novuscli_"$VERSION"_darwin-amd64"
mkdir ./usr ./DEBIAN && mkdir ./usr/local && mkdir ./usr/local/bin && mkdir ./usr/local/share && mkdir ./usr/local/share/man && mkdir ./usr/local/share/man/man1
cp ../nvs/target/release/nvs ./usr/local/bin
cp ../nvs/contrib/man/novuscli.1 ./usr/local/share/man/man1
touch DEBIAN/control
find . -name .DS_Store -type f -delete
chmod 0755 ./DEBIAN ./DEBIAN/* ./usr/local/bin/nvs
SIZE=$(du -sk ./usr | awk '{print $1}')
echo "Package: novuscli
Version: "$VERSION"
Architecture: darwin-amd64
Priority: optional
Section: Addons
Maintainer: Nikan Radan (SmushyTaco) <smushytaco@gmail.com>
Installed-Size: "$SIZE"
Depends: apt, sensible-utils, serna-apt-extra
Homepage: https://sernarepo.com
Description: Novus Command Line Edition is a modern CLI APT frontend.
" >> ./DEBIAN/control
cd .. && find . -name .DS_Store -type f -delete
dpkg-deb -b "novuscli_"$VERSION"_darwin-amd64" 1> /dev/null
rm -rf "novuscli_"$VERSION"_darwin-amd64"
echo "Done."
