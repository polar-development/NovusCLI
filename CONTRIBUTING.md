# Contributing to NovusCLI

As NovusCLI is a Polar Team product, we have some guidelines to follow in order for code to be the best experience it
can be and keep it as readable and as safe as possible:

#### We support the following licenses:

* MIT
* Apache
* BSD-License
* The Unlicense

#### Please don't contribute code under this licenses:

* GPL
* AGPL

#### Ethics & Codestyle:

* Feel free to check Codestyle here.
* Feel free to check for Ethis here.
