extern crate unicase;
use std::process::Command;
use unicase::UniCase;
use std::process;
use std::env;
pub fn help_menu() {
	//Prints help menu.
	if cfg!(target_os = "linux") || cfg!(target_os = "windows") {
		//NovusCLI Help
		println!("\n[1mNovusCLI Help[0m\n");
		//nvs search <query>
		println!("[1;35mnvs [0m[1;33msearch <query>[0m\t\t\tSearches for specified query");
		//nvs list [--flag(s)] <package(s)>
		println!("[1;35mnvs [0m[1;33mlist [--flag(s)] <package(s)>[0m\tLists specified packages");
		//nvs info <package(s)>
		println!("[1;35mnvs [0m[1;33minfo <package(s)>[0m\t\t\tDisplay info on specified package(s)");
		//nvs install <package(s)>
		println!("[1;35mnvs [0m[1;33minstall <package(s)>[0m\t\tInstalls specified package(s)");
		//nvs reinstall <package(s)>
		println!("[1;35mnvs [0m[1;33mreinstall <package(s)>[0m\t\tReinstalls specified package(s)");
		//nvs remove <package(s)>
		println!("[1;35mnvs [0m[1;33mremove <package(s)>[0m\t\t\tRemoves specified package(s)");
		//nvs add-key <filepath>
		println!("[1;35mnvs [0m[1;33madd-key <filepath>[0m\t\t\tAdds a key to the list of trusted keys");
		//nvs edit-sources
		println!("[1;35mnvs [0m[1;33medit-sources[0m\t\t\tOpens the APT repo editor");
		//nvs add-repositories [-a] <repo(s)>
		println!("[1;35mnvs [0m[1;33madd-repositories [-a] <repo(s)>[0m\tAdds new repo(s)");
		//nvs autoremove
		println!("[1;35mnvs [0m[1;33mautoremove[0m\t\t\t\tRemoves unneeded packages (orphans)");
		//nvs update
		println!("[1;35mnvs [0m[1;33mupdate[0m\t\t\t\tUpdates the repository lists");
		//nvs upgrade <package(s)>
		println!("[1;35mnvs [0m[1;33mupgrade <package(s)>[0m\t\tUpgrades specified packages");
		//nvs full-upgrade <package(s)>
		println!("[1;35mnvs [0m[1;33mfull-upgrade <package(s)>[0m\t\tUpgrades the system (or package(s))");
		//nvs version
		println!("[1;35mnvs [0m[1;33mversion[0m\t\t\t\tDisplay APT, DPKG, and NovusCLI versions");
		//nvs clean
		println!("[1;35mnvs [0m[1;33mclean[0m\t\t\t\tClears the download cache");
		//nvs help <command>
		println!("[1;35mnvs [0m[1;33mhelp <command>[0m\t\t\tOpens help menu for specified commands");
		//nvs about
		println!("[1;35mnvs [0m[1;33mabout[0m\t\t\t\tView legal information and credits\n");
		process::exit(0);
	} else {
		//NovusCLI Help
		println!("\n[1mNovusCLI Help[0m\n");
		//nvs search <query>
		println!("[1;35mnvs [0m[1;33msearch <query>[0m\t\t\tSearches for specified query");
		//nvs list [--flag(s)] <package(s)>
		println!("[1;35mnvs [0m[1;33mlist [--flag(s)] <package(s)>[0m\tLists specified packages");
		//nvs info <package(s)>
		println!("[1;35mnvs [0m[1;33minfo <package(s)>[0m\t\t\tDisplay info on specified package(s)");
		//nvs install <package(s)>
		println!("[1;35mnvs [0m[1;33minstall <package(s)>[0m\t\tInstalls specified package(s)");
		//nvs reinstall <package(s)>
		println!("[1;35mnvs [0m[1;33mreinstall <package(s)>[0m\t\tReinstalls specified package(s)");
		//nvs remove <package(s)>
		println!("[1;35mnvs [0m[1;33mremove <package(s)>[0m\t\t\tRemoves specified package(s)");
		//nvs add-key <filepath>
		println!("[1;35mnvs [0m[1;33madd-key <filepath>[0m\t\t\tAdds a key to the list of trusted keys");
		//nvs edit-sources
		println!("[1;35mnvs [0m[1;33medit-sources[0m\t\t\tOpens the APT repo editor");
		//nvs add-repositories [-a] <repo(s)>
		println!("[1;35mnvs [0m[1;33madd-repositories [-a] <repo(s)>[0m\tAdds new repo(s)");
		//nvs autoremove
		println!("[1;35mnvs [0m[1;33mautoremove[0m\t\t\t\tRemoves unneeded packages (orphans)");
		//nvs update
		println!("[1;35mnvs [0m[1;33mupdate[0m\t\t\t\tUpdates the repository lists");
		//nvs upgrade <package(s)>
		println!("[1;35mnvs [0m[1;33mupgrade <package(s)>[0m\t\tUpgrades specified packages");
		//nvs version
		println!("[1;35mnvs [0m[1;33mversion[0m\t\t\t\tDisplay APT, DPKG, and NovusCLI versions");
		//nvs clean
		println!("[1;35mnvs [0m[1;33mclean[0m\t\t\t\tClears the download cache");
		//nvs help <command>
		println!("[1;35mnvs [0m[1;33mhelp <command>[0m\t\t\tOpens help menu for specified commands");
		//nvs about
		println!("[1;35mnvs [0m[1;33mabout[0m\t\t\t\tView legal information and credits\n");
		process::exit(0);
	}
}
pub fn if_checks_for_things_with_one_argument() {
	let args: Vec<String> = env::args().collect();
	if UniCase::new(&args[1]) == UniCase::new("list") {
		//Runs apt list
		if cfg!(target_os = "windows") {
			Command::new("wsl").arg("--exec").arg("apt").arg("list").status().expect("Something went wrong.");
		} else {
			Command::new("apt").arg("list").status().expect("Something went wrong.");
		}
		process::exit(0);
	} else if UniCase::new(&args[1]) == UniCase::new("autoremove") {
		//Runs sudo apt purge --autoremove
		if cfg!(target_os = "windows") {
			Command::new("wsl").arg("--exec").arg("sudo").arg("apt").arg("purge").arg("--autoremove").status().expect("Something went wrong.");
		} else {
			Command::new("sudo").arg("apt").arg("purge").arg("--autoremove").status().expect("Something went wrong.");
		}
		process::exit(0);
	} else if UniCase::new(&args[1]) == UniCase::new("update") || UniCase::new(&args[1]) == UniCase::new("edit-sources") {
		//Runs sudo apt + first argument user inputted.
		if cfg!(target_os = "windows") {
			Command::new("wsl").arg("--exec").arg("sudo").arg("apt").arg(&args[1].to_lowercase()).status().expect("Something went wrong.");
		} else {
			Command::new("sudo").arg("apt").arg(&args[1].to_lowercase()).status().expect("Something went wrong.");
		}
		process::exit(0);
	} else if UniCase::new(&args[1]) == UniCase::new("upgrade") {
		//Runs sudo apt update
		Command::new("sudo").arg("apt").arg("update").status().expect("Something went wrong.");
		//Runs sudo apt upgrade
		Command::new("sudo").arg("apt").arg("upgrade").status().expect("Something went wrong.");
		process::exit(0);
	} else if UniCase::new(&args[1]) == UniCase::new("full-upgrade") {
		if cfg!(target_os = "linux") {
			//Runs sudo apt update
			Command::new("sudo").arg("apt").arg("update").status().expect("Something went wrong.");
			//Runs sudo apt upgrade
			Command::new("sudo").arg("apt").arg("full-upgrade").status().expect("Something went wrong.");
			process::exit(0);
		} else if cfg!(target_os = "windows") {
			//Runs sudo apt update
			Command::new("wsl").arg("--exec").arg("sudo").arg("apt").arg("update").status().expect("Something went wrong.");
			//Runs sudo apt upgrade
			Command::new("wsl").arg("--exec").arg("sudo").arg("apt").arg("full-upgrade").status().expect("Something went wrong.");
			process::exit(0);
		} else {
			//Displays this message if the command the user entered doesn't exist.
			eprintln!("Unknown command {:?}, type \"nvs help\" to see the list of available commands.", args[1].to_lowercase());
			process::exit(1);
		}
	} else if UniCase::new(&args[1]) == UniCase::new("clean") {
		if cfg!(target_os = "windows") {
			//Runs sudo apt autoclean
			Command::new("wsl").arg("--exec").arg("sudo").arg("apt").arg("autoclean").status().expect("Something went wrong.");
			//Runs sudo apt clean
			Command::new("wsl").arg("--exec").arg("sudo").arg("apt").arg("clean").status().expect("Something went wrong.");
		} else {
			//Runs sudo apt autoclean
			Command::new("sudo").arg("apt").arg("autoclean").status().expect("Something went wrong.");
			//Runs sudo apt clean
			Command::new("sudo").arg("apt").arg("clean").status().expect("Something went wrong.");
		}
		process::exit(0);
	} else if UniCase::new(&args[1]) == UniCase::new("help") {
		help_menu();
	} else if UniCase::new(&args[1]) == UniCase::new("about") {
		//Prints about message.
		println!("About NovusCLI");
		println!("Copyright (C) 2019 Polar Development.");
		println!("https://www.polardev.org/\n");
		println!("Copyright 2019 Polar Team");
		println!("Licensed under the Apache License, Version 2.0 (the \"License\");");
		println!("you may not use this file except in compliance with the License.");
		println!("You may obtain a copy of the License at\n");
		println!("http://www.apache.org/licenses/LICENSE-2.0");
		println!("Unless required by applicable law or agreed to in writing, software");
		println!("distributed under the License is distributed on an \"AS IS\" BASIS,");
		println!("WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.");
		println!("See the License for the specific language governing permissions and");
		println!("limitations under the License.\n");
		println!("NovusCLI is made possible by the following software and people:\n");
		println!("Nikan Radan (SmushyTaco) for writing it");
		println!("Debian for creating APT: https://packages.debian.org/stretch/apt");
		println!("Canonical for maintaining the latest versions of APT: https://launchpad.net/ubuntu/+source/apt/");
		println!("Diego Magdaleno & Hayden Seay (Diatrus) for hosting it on Project Serna: https://sernarepo.com/");
		process::exit(0);
	} else if UniCase::new(&args[1]) == UniCase::new("version") {
		//Displays the current version of NovusCLI the user is running.
		println!("NovusCLI 0.9.5\n");
		if cfg!(target_os = "windows") {
			//Displays the version of apt the user is running.
			Command::new("wsl").arg("--exec").arg("apt").arg("--version").status().expect("Something went wrong.");
			println!("");
			//Displays the version of apt the user is running.
			Command::new("wsl").arg("--exec").arg("dpkg").arg("--version").status().expect("Something went wrong.");
		} else {
			//Displays the version of apt the user is running.
			Command::new("apt").arg("--version").status().expect("Something went wrong.");
			println!("");
			//Displays the version of apt the user is running.
			Command::new("dpkg").arg("--version").status().expect("Something went wrong.");
		}
		process::exit(0);
	} else if UniCase::new(&args[1]) == UniCase::new("install") || UniCase::new(&args[1]) == UniCase::new("reinstall") || UniCase::new(&args[1]) == UniCase::new("remove") || UniCase::new(&args[1]) == UniCase::new("info") || UniCase::new(&args[1]) == UniCase::new("search") || UniCase::new(&args[1]) == UniCase::new("add-repositories") {
		//Gives error message if only 1 argument was inputted for something that requires 2 arguments.
		eprintln!("{:?} needs at least two arguments but you only entered one, run \"nvs help {}\" to learn how to properly use {:?}.", args[1].to_lowercase(), args[1].to_lowercase(), args[1].to_lowercase());
		process::exit(1);
	} else {
		//Displays this message if the command the user entered doesn't exist.
		eprintln!("Unknown command {:?}, type \"nvs help\" to see the list of available commands.", args[1].to_lowercase());
		process::exit(1);
	}
}
