extern crate unicase;
use crate::one_argument_envirnment_variable_commands::help_menu;
use crate::one_argument_envirnment_variable_commands::if_checks_for_things_with_one_argument;
use std::process::Command;
use unicase::UniCase;
use std::process;
use std::env;
pub fn if_checks_for_things_with_two_or_more_arguments() {
	let args: Vec<String> = env::args().collect();
	if UniCase::new(&args[1]) == UniCase::new("search") || UniCase::new(&args[1]) == UniCase::new("info") {
		//Runs apt + first argument user inputted as lowercase + all other arguments.
		if cfg!(target_os = "windows") {
			Command::new("wsl").arg("--exec").arg("apt").arg(&args[1].to_lowercase()).args(&args[2..]).status().expect("Something went wrong.");
		} else {
			Command::new("apt").arg(&args[1].to_lowercase()).args(&args[2..]).status().expect("Something went wrong.");
		}
		process::exit(0);
	} else if UniCase::new(&args[1]) == UniCase::new("install") || UniCase::new(&args[1]) == UniCase::new("reinstall") {
		if cfg!(target_os = "windows") {
			//Runs sudo apt update
			Command::new("wsl").arg("--exec").arg("sudo").arg("apt").arg("update").status().expect("Something went wrong.");
			//Runs sudo apt + first argument user inputted as lowercase + all other arguments.
			Command::new("wsl").arg("--exec").arg("sudo").arg("apt").arg(&args[1].to_lowercase()).args(&args[2..]).status().expect("Something went wrong.");
		} else {
			//Runs sudo apt update
			Command::new("sudo").arg("apt").arg("update").status().expect("Something went wrong.");
			//Runs sudo apt + first argument user inputted as lowercase + all other arguments.
			Command::new("sudo").arg("apt").arg(&args[1].to_lowercase()).args(&args[2..]).status().expect("Something went wrong.");
		}
		process::exit(0);
	} else if UniCase::new(&args[1]) == UniCase::new("remove") {
		//Runs sudo apt purge + all other arguments.
		if cfg!(target_os = "windows") {
			Command::new("wsl").arg("--exec").arg("sudo").arg("apt").arg("purge").args(&args[2..]).status().expect("Something went wrong.");
		} else {
			Command::new("sudo").arg("apt").arg("purge").args(&args[2..]).status().expect("Something went wrong.");
		}
		process::exit(0);
	} else if UniCase::new(&args[1]) == UniCase::new("add-key") {
		//Runs sudo apt-key add + second argument the user inputted.
		if cfg!(target_os = "windows") {
			Command::new("wsl").arg("--exec").arg("sudo").arg("apt-key").arg("add").arg(&args[2]).status().expect("Something went wrong.");
		} else {
			Command::new("sudo").arg("apt-key").arg("add").arg(&args[2]).status().expect("Something went wrong.");
		}
		process::exit(0);
	} else if UniCase::new(&args[1]) == UniCase::new("upgrade") {
		if cfg!(target_os = "windows") {
			//Runs sudo apt update
			Command::new("wsl").arg("--exec").arg("sudo").arg("apt").arg("update").status().expect("Something went wrong.");
			//Runs sudo apt upgrade + all other arguments.
			Command::new("wsl").arg("--exec").arg("sudo").arg("apt").arg("upgrade").args(&args[2..]).status().expect("Something went wrong.");
		} else {
			//Runs sudo apt update
			Command::new("sudo").arg("apt").arg("update").status().expect("Something went wrong.");
			//Runs sudo apt upgrade + all other arguments.
			Command::new("sudo").arg("apt").arg("upgrade").args(&args[2..]).status().expect("Something went wrong.");
		}
		process::exit(0);
	} else if UniCase::new(&args[1]) == UniCase::new("full-upgrade") {
		if cfg!(target_os = "linux") {
			//Runs sudo apt update
			Command::new("sudo").arg("apt").arg("update").status().expect("Something went wrong.");
			//Runs sudo apt upgrade
			Command::new("sudo").arg("apt").arg("full-upgrade").args(&args[2..]).status().expect("Something went wrong.");
			process::exit(0);
		} else if cfg!(target_os = "windows") {
			//Runs sudo apt update
			Command::new("wsl").arg("--exec").arg("sudo").arg("apt").arg("update").status().expect("Something went wrong.");
			//Runs sudo apt upgrade
			Command::new("wsl").arg("--exec").arg("sudo").arg("apt").arg("full-upgrade").args(&args[2..]).status().expect("Something went wrong.");
			process::exit(0);
		} else {
			//Displays this message if the command the user entered doesn't exist.
			eprintln!("Unknown command {:?}, type \"nvs help\" to see the list of available commands.", args[1].to_lowercase());
			process::exit(1);
		}
	} else if UniCase::new(&args[1]) == UniCase::new("add-repositories") {
		//Runs add-repositories + all other arguments.
		if cfg!(target_os = "windows") {
			Command::new("wsl").arg("--exec").arg("add-apt-repositories").args(&args[2..]).status().expect("Something went wrong.");
			process::exit(0);
		} else {
			Command::new("add-apt-repositories").args(&args[2..]).status().expect("Something went wrong.");
			process::exit(0);
		}
	} else if UniCase::new(&args[1]) == UniCase::new("list") {
		//Runs apt list
		if cfg!(target_os = "windows") {
			Command::new("wsl").arg("--exec").arg("apt").arg("list").args(&args[2..]).status().expect("Something went wrong.");
		} else {
			Command::new("apt").arg("list").args(&args[2..]).status().expect("Something went wrong.");
		}
		process::exit(0);
	} else if UniCase::new(&args[1]) == UniCase::new("help") {
		if UniCase::new(&args[2]) == UniCase::new("search") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs search
			println!("\t[1;35mnvs [0m[1;33msearch[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs search <query>
			println!("\t[1;35mnvs [0m[1;33msearch <query>[0m");
			//nvs search <queries>
			println!("\t[1;35mnvs [0m[1;33msearch <queries>[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs search command is used to search for the specified package(s)
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33msearch [0m[37mcommand is used to search for the specified package(s)");
			//throughout your APT repositories. When using nvs search you can input
			println!("\tthroughout your APT repositories. When using [0m[1;35mnvs [0m[1;33msearch [0m[37myou can input");
			//one or more search parameters.
			println!("\tone or more search parameters.[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs search apt
			println!("\t[1;35mnvs [0m[1;33msearch apt[0m");
			//nvs search ap t
			println!("\t[1;35mnvs [0m[1;33msearch ap t[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("list") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs list
			println!("\t[1;35mnvs [0m[1;33mlist[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs list
			println!("\t[1;35mnvs [0m[1;33mlist[0m");
			//nvs list [--flag(s)] <package(s)>
			println!("\t[1;35mnvs [0m[1;33mlist [--flag(s)] <package(s)>[0m");
			//nvs list <package(s)> [--flag(s)]
			println!("\t[1;35mnvs [0m[1;33mlist <package(s)> [--flag(s)][0m");
			//nvs list <package(s)>
			println!("\t[1;35mnvs [0m[1;33mlist <package(s)>[0m");
			//nvs list [--flag(s)]
			println!("\t[1;35mnvs [0m[1;33mlist [--flag(s)][0m\n");
			//FLAGS
			println!("[1mFLAGS[0m");
			//nvs list --installed
			println!("\t[1;35mnvs [0m[1;33mlist --installed[0m");
			//nvs list -i
			println!("\t[1;35mnvs [0m[1;33mlist -i[0m");
			//nvs list --upgradable
			println!("\t[1;35mnvs [0m[1;33mlist --upgradable[0m");
			//nvs list -u
			println!("\t[1;35mnvs [0m[1;33mlist -u[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs list command is used to make a list of packages that are in all
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33mlist [0m[37mcommand is used to make a list of packages that are in all");
			//of your repositories. It can be used with or without arguments (as seen
			println!("\tof your repositories. It can be used with or without arguments (as seen");
			//under USAGE). Based on what arguments are (or aren't) inputted your
			println!("\tunder [0m[1mUSAGE [0m[37m). Based on what arguments are (or aren't) inputted your");
			//package list may differ.
			println!("\tpackage list may differ.[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs list
			println!("\t[1;35mnvs [0m[1;33mlist[0m");
			//nvs list --installed apt
			println!("\t[1;35mnvs [0m[1;33mlist --installed apt[0m");
			//nvs list apt -u
			println!("\t[1;35mnvs [0m[1;33mlist apt -u[0m");
			//nvs list apt dpkg
			println!("\t[1;35mnvs [0m[1;33mlist apt dpkg[0m");
			//nvs list -i
			println!("\t[1;35mnvs [0m[1;33mlist -i[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("info") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs info
			println!("\t[1;35mnvs [0m[1;33minfo[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs info <package(s)>
			println!("\t[1;35mnvs [0m[1;33minfo <package(s)>[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs info command is used to search for the specified package(s)
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33minfo [0m[37mcommand is used to search for the specified package(s)");
			println!("\tthroughout your APT repositories and display the information for each");
			//package. When using nvs info you can input one or more search
			println!("\tpackage. When using [0m[1;35mnvs [0m[1;33minfo [0m[37myou can input one or more search");
			//parameters.
			println!("\tparameters.[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs info apt
			println!("\t[1;35mnvs [0m[1;33minfo apt[0m");
			//nvs info apt dpkg
			println!("\t[1;35mnvs [0m[1;33minfo apt dpkg[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("install") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs install
			println!("\t[1;35mnvs [0m[1;33minstall[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs install <package(s)>
			println!("\t[1;35mnvs [0m[1;33minstall <package(s)>[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs install command is used to install the package(s) you specified
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33minstall [0m[37mcommand is used to install the package(s) you specified");
			//from your list of repositories. When using nvs install you can input one
			println!("\tfrom your list of repositories. When using [0m[1;35mnvs [0m[1;33minstall [0m[37myou can input one");
			//or more packages to install.
			println!("\tor more packages to install.[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs install novuscli
			println!("\t[1;35mnvs [0m[1;33minstall novuscli[0m");
			//nvs install zsh novuscli
			println!("\t[1;35mnvs [0m[1;33minstall zsh novuscli[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("reinstall") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs reinstall
			println!("\t[1;35mnvs [0m[1;33mreinstall[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs reinstall <package(s)>
			println!("\t[1;35mnvs [0m[1;33mreinstall <package(s)>[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs reinstall command is used to install the package(s) you
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33mreinstall [0m[37mcommand is used to reinstall the package(s) you");
			//specified from your list of repositories. When using nvs reinstall you
			println!("\tspecified from your list of repositories. When using [0m[1;35mnvs [0m[1;33mreinstall [0m[37myou");
			//can input one or more packages to reinstall.
			println!("\tcan input one or more packages to reinstall.[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs reinstall novuscli
			println!("\t[1;35mnvs [0m[1;33mreinstall novuscli[0m");
			//nvs reinstall zsh novuscli
			println!("\t[1;35mnvs [0m[1;33mreinstall zsh novuscli[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("remove") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs remove
			println!("\t[1;35mnvs [0m[1;33mremove[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs remove <package(s)>
			println!("\t[1;35mnvs [0m[1;33mremove <package(s)>[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs remove command is used to remove the package(s) you have
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33mremove [0m[37mcommand is used to remove the package(s) you have");
			//installed. When using nvs remove you can input one or more packages to
			println!("\tinstalled. When using [0m[1;35mnvs [0m[1;33mremove [0m[37myou can input one or more packages to");
			//remove.
			println!("\tremove.[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs remove python3
			println!("\t[1;35mnvs [0m[1;33mremove python3[0m");
			//nvs remove zsh python3
			println!("\t[1;35mnvs [0m[1;33mremove zsh python3[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("add-key") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs add-key
			println!("\t[1;35mnvs [0m[1;33madd-key[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs add-key <filepath>
			println!("\t[1;35mnvs [0m[1;33madd-key <filepath>[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs add-key command is used to add a GPG key to the list of trusted
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33madd-key [0m[37mcommand is used to add a GPG key to the list of trusted");
			//keys.
			println!("\tkeys.[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs add-key /folder/folder/destination.gpg
			println!("\t[1;35mnvs [0m[1;33madd-key /folder/folder/destination.gpg[0m");
			//nvs add-key /folder/folder/destination.asc
			println!("\t[1;35mnvs [0m[1;33madd-key /folder/folder/destination.asc[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("edit-sources") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs edit-sources
			println!("\t[1;35mnvs [0m[1;33medit-sources[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs edit-sources
			println!("\t[1;35mnvs [0m[1;33medit-sources[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs edit-sources command is used to open the file containing your
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33medit-sources [0m[37mcommand is used to open the file containing your");
			//APT sources with a command-line text editor.
			println!("\tAPT sources with a command-line text editor.[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs edit-sources
			println!("\t[1;35mnvs [0m[1;33medit-sources[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("autoremove") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs autoremove
			println!("\t[1;35mnvs [0m[1;33mautoremove[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs autoremove
			println!("\t[1;35mnvs [0m[1;33mautoremove[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs autoremove command is used to automatically remove unneeded
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33mautoremove [0m[37mcommand is used to automatically remove unneeded");
			//packages (orphans).
			println!("\tpackages (orphans).[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs autoremove
			println!("\t[1;35mnvs [0m[1;33mautoremove[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("update") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs update
			println!("\t[1;35mnvs [0m[1;33mupdate[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs update
			println!("\t[1;35mnvs [0m[1;33mupdate[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs update command is used to check your repositories and create a
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33mupdate [0m[37mcommand is used to check your repositories and create a");
			//new package list based on any changes.
			println!("\tnew package list based on any changes.[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs update
			println!("\t[1;35mnvs [0m[1;33mupdate[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("upgrade") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs upgrade
			println!("\t[1;35mnvs [0m[1;33mupgrade[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs upgrade
			println!("\t[1;35mnvs [0m[1;33mupgrade[0m");
			//nvs upgrade <package(s)>
			println!("\t[1;35mnvs [0m[1;33mupgrade <package(s)>[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs upgrade command is used to upgrade the package(s) you specified
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33mupgrade [0m[37mcommand is used to upgrade the package(s) you specified");
			println!("\t(if no packages were specified then it will upgrade everything that can");
			//be upgraded) from your list of repositories. When using nvs upgrade you
			println!("\tbe upgraded) from your list of repositories. When using [0m[1;35mnvs [0m[1;33mupgrade [0m[37myou");
			//can input zero or more packages to upgrade.
			println!("\tcan input zero or more packages to upgrade.[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs upgrade
			println!("\t[1;35mnvs [0m[1;33mupgrade[0m");
			//nvs upgrade novuscli
			println!("\t[1;35mnvs [0m[1;33mupgrade novuscli[0m");
			//nvs upgrade zsh novuscli
			println!("\t[1;35mnvs [0m[1;33mupgrade zsh novuscli[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("full-upgrade") {
			if cfg!(target_os = "linux") || cfg!(target_os = "windows") {
				//NAME
				println!("\n[1mNAME[0m");
				//nvs full-upgrade
				println!("\t[1;35mnvs [0m[1;33mfull-upgrade[0m\n");
				//USAGE
				println!("[1mUSAGE[0m");
				//nvs full-upgrade
				println!("\t[1;35mnvs [0m[1;33mfull-upgrade[0m");
				//nvs full-upgrade <package(s)>
				println!("\t[1;35mnvs [0m[1;33mfull-upgrade <package(s)>[0m\n");
				//DESCRIPTION
				println!("[1mDESCRIPTION[0m");
				//The nvs full-upgrade command is used to upgrade the package(s) you
				println!("\t[37mThe [0m[1;35mnvs [0m[1;33mfull-upgrade [0m[37mcommand is used to upgrade the package(s) you");
				println!("\tspecified (if no packages were specified then it will upgrade your linux");
				println!("\tdistrobution) from your list of repositories. When using");
				//nvs full-upgrade you can input zero or more packages to upgrade.
				println!("\t[0m[1;35mnvs [0m[1;33mfull-upgrade [0m[37myou can input zero or more packages to upgrade.[0m");
				//EXAMPLES
				println!("[1mEXAMPLES[0m");
				//nvs full-upgrade
				println!("\t[1;35mnvs [0m[1;33mfull-pgrade[0m");
				//nvs full-upgrade novuscli
				println!("\t[1;35mnvs [0m[1;33mfull-upgrade novuscli[0m");
				//nvs full-upgrade zsh novuscli
				println!("\t[1;35mnvs [0m[1;33mfull-upgrade zsh novuscli[0m\n");
				process::exit(0);
			} else {
				//NovusCLI Help
				eprintln!("\n[1mNovusCLI Help[0m\n");
				//nvs search <query>
				eprintln!("[1;35mnvs [0m[1;33msearch <query>[0m\t\t\tSearches for specified query");
				//nvs list [--flag(s)] <package(s)>
				eprintln!("[1;35mnvs [0m[1;33mlist [--flag(s)] <package(s)>[0m\tLists specified packages");
				//nvs info <package(s)>
				eprintln!("[1;35mnvs [0m[1;33minfo <package(s)>[0m\t\t\tDisplay info on specified package(s)");
				//nvs install <package(s)>
				eprintln!("[1;35mnvs [0m[1;33minstall <package(s)>[0m\t\tInstalls specified package(s)");
				//nvs reinstall <package(s)>
				eprintln!("[1;35mnvs [0m[1;33mreinstall <package(s)>[0m\t\tReinstalls specified package(s)");
				//nvs remove <package(s)>
				eprintln!("[1;35mnvs [0m[1;33mremove <package(s)>[0m\t\t\tRemoves specified package(s)");
				//nvs add-key <filepath>
				eprintln!("[1;35mnvs [0m[1;33madd-key <filepath>[0m\t\t\tAdds a key to the list of trusted keys");
				//nvs edit-sources
				eprintln!("[1;35mnvs [0m[1;33medit-sources[0m\t\t\tOpens the APT repo editor");
				//nvs add-repositories [-a] <repo(s)>
				eprintln!("[1;35mnvs [0m[1;33madd-repositories [-a] <repo(s)>[0m\tAdds new repo(s)");
				//nvs autoremove
				eprintln!("[1;35mnvs [0m[1;33mautoremove[0m\t\t\t\tRemoves unneeded packages (orphans)");
				//nvs update
				eprintln!("[1;35mnvs [0m[1;33mupdate[0m\t\t\t\tUpdates the repository lists");
				//nvs upgrade <package(s)>
				eprintln!("[1;35mnvs [0m[1;33mupgrade <package(s)>[0m\t\tUpgrades specified packages");
				//nvs version
				eprintln!("[1;35mnvs [0m[1;33mversion[0m\t\t\t\tDisplay APT, DPKG, and NovusCLI versions");
				//nvs clean
				eprintln!("[1;35mnvs [0m[1;33mclean[0m\t\t\t\tClears the download cache");
				//nvs help <command>
				eprintln!("[1;35mnvs [0m[1;33mhelp <command>[0m\t\t\tOpens help menu for specified commands");
				//nvs about
				eprintln!("[1;35mnvs [0m[1;33mabout[0m\t\t\t\tView legal information and credits\n");
				process::exit(1);
			}
		} else if UniCase::new(&args[2]) == UniCase::new("version") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs version
			println!("\t[1;35mnvs [0m[1;33mversion[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs version
			println!("\t[1;35mnvs [0m[1;33mversion[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs version command is used to display the current version of APT,
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33mversion [0m[37mcommand is used to display the current version of APT,");
			//DPKG, and NovusCLI you're running.
			println!("\tDPKG, and NovusCLI you're running.[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs version
			println!("\t[1;35mnvs [0m[1;33mversion[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("clean") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs clean
			println!("\t[1;35mnvs [0m[1;33mclean[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs clean
			println!("\t[1;35mnvs [0m[1;33mclean[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs clean command is used to clean the download cache.
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33mclean [0m[37mcommand is used to clean the download cache.[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs version
			println!("\t[1;35mnvs [0m[1;33mclean[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("help") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs help
			println!("\t[1;35mnvs [0m[1;33mhelp[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs help
			println!("\t[1;35mnvs [0m[1;33mhelp[0m");
			//nvs help <command>
			println!("\t[1;35mnvs [0m[1;33mhelp <command>[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs help command is used to display the help menu for you. When
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33mhelp [0m[37mcommand is used to display the help menu for you. When");
			//using nvs help <command> you can input zero arguments or one argument.
			println!("\tusing [0m[1;35mnvs [0m[1;33mhelp <command> [0m[37myou can input zero arguments or one argument.[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs help
			println!("\t[1;35mnvs [0m[1;33mhelp[0m");
			//nvs help list
			println!("\t[1;35mnvs [0m[1;33mhelp list[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("about") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs about
			println!("\t[1;35mnvs [0m[1;33mabout[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs about
			println!("\t[1;35mnvs [0m[1;33mabout[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs about command is used to view legal information and credits
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33mabout [0m[37mcommand is used to view legal information and credits");
			//regarding NovusCLI.
			println!("\tregarding NovusCLI.[0m\n");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs about
			println!("\t[1;35mnvs [0m[1;33mabout[0m\n");
			process::exit(0);
		} else if UniCase::new(&args[2]) == UniCase::new("add-repositories") {
			//NAME
			println!("\n[1mNAME[0m");
			//nvs add-repositories
			println!("\t[1;35mnvs [0m[1;33madd-repositories[0m\n");
			//USAGE
			println!("[1mUSAGE[0m");
			//nvs add-repositories <repo(s)>
			println!("\t[1;35mnvs [0m[1;33madd-repositories <repo(s)>[0m\n");
			//nvs add-repositories [-a] <repo> <codename> <component>
			println!("\t[1;35mnvs [0m[1;33madd-repositories [-a] <repo> <codename> <component>[0m\n");
			//DESCRIPTION
			println!("[1mDESCRIPTION[0m");
			//The nvs add-repositories command is used to add new apt sources.
			println!("\t[37mThe [0m[1;35mnvs [0m[1;33madd-repositories [0m[37mcommand is used to add new apt repositories.");
			//EXAMPLES
			println!("[1mEXAMPLES[0m");
			//nvs add-repositories google.com
			println!("\t[1;35mnvs [0m[1;33madd-repositories google.com[0m\n");
			//nvs add-repositories google.com polardev.org
			println!("\t[1;35mnvs [0m[1;33madd-repositories google.com polardev.org[0m\n");
			//nvs add-repositories -a google.com catalina main
			println!("\t[1;35mnvs [0m[1;33madd-repositories -a google.com catalina main[0m\n");
			//nvs add-repositories --advanced google.com catalina main
			println!("\t[1;35mnvs [0m[1;33madd-repositories --advanced google.com catalina main[0m\n");
			//nvs add-repositories -a google.com catalina main polardev.org
			println!("\t[1;35mnvs [0m[1;33madd-repositories -a google.com catalina main polardev.org[0m\n");
			//nvs add-repositories --advanced google.com catalina main polardev.org
			println!("\t[1;35mnvs [0m[1;33madd-repositories --advanced google.com catalina main polardev.org[0m\n");
			process::exit(0);
		} else {
		help_menu();
		}
	} else {
		//This is here so if a one argument command was typed with two or more arguments it'll be checked here and executed as should.
		if_checks_for_things_with_one_argument();
	}
}
